# Acorn A3010 External Floppy Mod

Adds a floppy drive outlet for an external floppy drive, like a Gotek.

This contains 3 sub projects:

## Arch 2nd Floppy  
(c) 2018 Simon Inns  
A PCB to allow a second drive on the Acorn A3010.  
See also the [The original thread on Stardot](https://stardot.org.uk/forums/viewtopic.php?f=16&t=14874) where Simon introduced this project.


* To be opened with KiCad 4.x.
    - [KiCad 4.07, Windows](https://downloads.kicad.org/kicad/windows/explore/stable/download/kicad-4.0.7-i686.exe)   
    - [KiCad 4.07, macOS](https://downloads.kicad.org/kicad/macos/explore/stable/download/kicad-extras-4.0.7.dmg)  
    - Other OS: [https://www.kicad.org/download/](https://www.kicad.org/download/) (may not feature version 4.x)  

    Newer KiCad versions have a revised components library and might not add the right components after importing this project.

![Archimedes 2nd Floppy PCB by Simon Inns](readme-images/2ndfloppy.png)  

See also the [Riscworld Vol 3, Issue 1, Floppy-Article](http://discmaster.textfiles.com/view/16378/RISCWORLD_VOL3.iso/HTML/ISSUE1/FLOPPY/INDEX.HTM) by David Holden where the idea and schematics are from:  
> ![Schema](readme-images/2nd_floppy_schema.gif)
>
> "The base of T1 is connected to the DS 0 line, so when this is high (NOT DS 0) the collector of T1 pulls DS 1 low.  
> This would mean that drive 1 was selected whenever drive 0 wasn't, which would be most of the time, so T2 is connected to the MOTOR ON line, and when this is high (motor off) it turns on and switches T2 off, ensuring that drive 1 is only selected when drive 1 is OFF and the motor is ON."  
>
> © Copyright 2003 APDL. RISC World is published by APDL and edited by Aaron Timbrell.

## Backpanel A3010 Connector PCB  

(c) 2022 Wanja Gayk  
A PCB to add connectors for the Floppy ribbon cable and a Floppy power cable. It will add  external and internal jacks, so you can connect the back panel like an internal floppy drive, assemble your Archimedes computer and connect the floppy just like you would do it internally. This is to avoid internal cables routed to the outside.  
    
* The files for this PCB are created and should be opened using KiCad 6:
    - [https://www.kicad.org/download/](https://www.kicad.org/download/)

![External Floppy for Archimedes A3010 Connector PCB by Wanja Gayk](readme-images/panel_pcb.png)

## Backpanel A3010
(c) 2022 Wanja Gayk  
This is a 3D-model of a back plane with cut-outs and stays to fix the Connector PCB to.  
* The Model was created and should be opened using FreeCAD 0.19:
    - [https://www.freecadweb.org/](https://www.freecadweb.org/)

![External Floppy for Archimedes A3010 Panel by Wanja Gayk](readme-images/panel.png)

I did not create any holes for screws to make it more stable, the idea is to just glue it on.  
The back panel and the PCB are functionally connected and all cables are pluggable anyway. There is no reason to design for any disassembly and reassembly. However, you might try to use a removable double-sided tape, such as the Scotch 2002-CFT.

![External Floppy for Archimedes A3010, Panel and PCB assembly](readme-images/Assembly.png)

# PCB manufacturing

I have added the Gerber and drill files to the "PCBway" directory of the projects. There is a zip-file that you can just upload. Make sure to choose a lead free option, as the EU prohibits the import of leaded PCBs. Here's a list of possible PCB manufacturers:

* [JLCPCB](https://jlcpcb.com) - I had them manufacture my own boards. Easy website to use, low prices, very fast shipping. Has 5 Factories across China, plus contact offices in Hongkong, Shenzhen and Krefeld/Germany.
* [PCBWay](https://www.pcbway.com) - Probably the most well-known service from Shenzhen/China, Service offices in Hongkong and Charenton le Pont/France, Hotlines in French, Spanish, Spanish, Russian and Chinese.
* [Beta Layout](https://de.beta-layout.com/leiterplatten/) - German manufacturer with Overseas partners. The website is the easiest to use, but it's unfortunately pretty pricey.
* [Elecrow](https://www.elecrow.com/pcb-manufacturing.html) - Another Chinese service.

# 3D Printing

The Backpanel A3010 directory contains an ```A3010_backpanel-Body (Meshed).stl``` file, that can be uploaded to a lot of 3D-Printing web sites. If you're the lucky owner of your own 3d Printer, you know more than I do. For everyone else there are services like these:

* [3DDesign24.de](https://www.3ddesign24.de/produkt/3d-druck-service/) This is where I had my first part produced. German manufacturer from Wuppertal. Very low prices, easy to use, but German language web site with a small number of affordable materials (PLA, PETG, ABS and TPU). Has subscription services that will guarantee you delivery within a week at no shipping costs. Subscriptions will last 3 or 4 months minimum, depending on the subscription. You can go without a subscription though. However, the part I got (PETG, finest resolution, 100% infill) was of pretty bad quality, i.e. a very rough finish that needs a lot of grinding, the infill does seem to be 100% and you could peel off some strands, so I cannot recommend this method or this service.

* [Sculpteo](https://www.sculpteo.com) German Manufacturer owned by BASF. Pretty usual prices. You need to log in in via Google/Facebook/etc, which will create an account on their website. If you just upload your part to get a quote, you'll be nagged with mails later. But there's a very good feature: Their 3D-View features a "cut"- and a "stability test"-view that helps analysing your part and identifying problematic areas for any material. Minimum order is 30€ (which is 2 panels for SLS Nylon PA12), plus 10€ shipping. I had the rest of my panels made by them. The finish has a rough sand-blasted texture, but the dimensions and edges are very accurate. This should give a pretty decent finish and plenty of grip to glue the PCB to the brackets. Lightly sanded, pre-painted with some filler you should get a texture that blends in pretty well with the rest of the case.

* [Shapeways](https://www.shapeways.com) Pretty well-known manufacturer with HQ and manufacturing in New York City/USA and another manufacturing plant in Eindhoven in the Netherlands. Base price is okay, but gets expensive quick on any extras and there is not a lot of choice of cheap materials. Will nag you with mails once you've tried to get a quote.

* [Craftcloud](https://craftcloud3d.com/upload) A search engine to get you quotes from a plethora of manufacturers based you your location. It did not find me the best price, but a few competitive ones. Probably a good starting point.

# Gotek setup

Prior to using anything of this mod, first you need to get your Gotek running as the only device of your Acorn A3010.  
This can be a bit of a nightmare and to be honest I forgot almost everything I did regarding the config. There is plenty of documentation around though.  
However, once you've got this drive running, the most important thing is the Gotek's "Drive Select" jumper. The first device on your cable (Drive 0) will be the original internal floppy drive, the second one will be your back plane.
Your Gotek will connect to the back plane with a straight, not twisted, IDC cable. Just like this one from [AmigaStore.eu](https://amigastore.eu/en/430-floppy-drive-data-and-power-cable.html), or the original cables from inside your Archimedes A3010. Once plugged in, you need to set the Gotek's "Drive Select" jumper to S1 (aka. "DS1").  There are several versions of the Gotek board, the [Simulant Gotek Drive](https://www.simulant.uk/shop/Gotek-Floppy-Disk-Drive-Emulators/Gotek-Amstrad-Amiga-Atari-Spectrum-DOS-IBM-PC-Floppy-disc-disk-drive-USB-emulator), that I have, has 4 possible jumper positions: Drive0, Drive1, Drive2, and MO.  
Here you can see the jumper set to Drive1:  
![Gotek jumper settings from left to right: DS0, DS1, DS2, MO](readme-images/Gotek_jumper.png)  
MO (=Motor Signal), the rightmost option in the picture, will "select" the drive with every detected host access (i.e. motor is on). This is not advised. It's an option only suitable, if the Gotek is your only drive. You will notice that your original drive's motor might start if you're accessing the second drive. This is normal, it's a possible side effect of Simon's "2nd Floppy" mod.

# Installation

The data cable runs from the main board (under the floppy) to the _Archimedes 2nd Floppy_ board, then into the internal drive, then to the _External Floppy Mod_ board:
![Data cable detail](readme-images/install.jpg)  

The power cable can be routed across the data cable.
![overview](readme-images/install_overview.jpg)  

You can use the original cables or any straight, not twisted, IDC cable and a power cable, like this one from [AmigaStore.eu](https://amigastore.eu/en/430-floppy-drive-data-and-power-cable.html) to connect your 2nd drive to the  back plane:
![Outside](readme-images/install_back.jpg)  

Set your floppies accordingly in the configuration settings. 
Note: Your external drive :1 will always be listed first.
![Config](readme-images/Config_Screenshot.jpg) 

# Disclaimer

The projects on this website have been tested and/or built by me or someone else who's project I'm using myself. Either way, I am not a professional electronic technician, electrical engineer or the like. The different trades on this website are strictly a hobby for me and I have either been self-taught and/or have learned different tools, tips and tricks from the internet. I may not know or follow all suggested safety tips that are out there, but I do try to use common sense, as you should too.  

I do not take any responsibility for any injuries or death that may occur from any accident, lack of common sense, stupidity, or the like, while working on the projects on this website or any project outside of this website. Furthermore, responsibility for any losses, damages or distress resulting from adherence to any information or hardware made available through or based on information from this website is not the responsibility of the creator of this website.
I have taken reasonable care to make sure that all information and hardware presented or made by myself from the information on this website are as accurate as possible at the time of publication, but I take no responsibility for any errors or omissions contained herein.  

I highly recommend that you follow all safety procedures that are provided with the tools and/or products that you will be using during the making of the projects on this website, or the hardware you purchased that was made by me as a result of the projects presented here. I recommend that you use common sense, wear the necessary safety protection and use the recommended safety devices for the tools you use. If you have any doubts in the electrical circuits presented, measure and test the device, before connecting it to your beloved hardware!  

Remember, these are hobbies for us. Most of all, have a great time building and creating!

## Now shut your case and get outta here.