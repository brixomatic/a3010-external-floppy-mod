EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Floppymod-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Acorn Archimedes 2nd Floppy Adapter"
Date "2018-04-01"
Rev "1_0"
Comp "https://www.waitingforfriday.com"
Comment1 "(c)2018 Simon Inns"
Comment2 "License: Attribution-ShareAlike 4.0 international (CC BY-SA 4.0)"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R1
U 1 1 5AAE0A10
P 6650 3300
F 0 "R1" V 6730 3300 50  0000 C CNN
F 1 "47K" V 6650 3300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6580 3300 50  0001 C CNN
F 3 "" H 6650 3300 50  0001 C CNN
	1    6650 3300
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 5AAE0A60
P 6650 3850
F 0 "R2" V 6730 3850 50  0000 C CNN
F 1 "47K" V 6650 3850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6580 3850 50  0001 C CNN
F 3 "" H 6650 3850 50  0001 C CNN
	1    6650 3850
	0    1    1    0   
$EndComp
Text GLabel 6400 3300 0    60   Input ~ 0
DRIVE0
Text GLabel 7750 3000 2    60   Input ~ 0
DRIVE1
Text GLabel 6400 3850 0    60   Input ~ 0
MOTOR_ON
$Comp
L GND #PWR01
U 1 1 5AAE0AE1
P 7500 3550
F 0 "#PWR01" H 7500 3300 50  0001 C CNN
F 1 "GND" H 7500 3400 50  0000 C CNN
F 2 "" H 7500 3550 50  0001 C CNN
F 3 "" H 7500 3550 50  0001 C CNN
	1    7500 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 3300 7200 3300
Wire Wire Line
	7500 3500 7500 3550
Wire Wire Line
	6400 3300 6500 3300
Wire Wire Line
	7500 3100 7500 3000
Wire Wire Line
	7500 3000 7750 3000
Wire Wire Line
	6400 3850 6500 3850
Wire Wire Line
	6800 3850 6850 3850
Wire Wire Line
	7150 3650 7150 3300
Connection ~ 7150 3300
$Comp
L GND #PWR02
U 1 1 5AAE0BFA
P 7150 4100
F 0 "#PWR02" H 7150 3850 50  0001 C CNN
F 1 "GND" H 7150 3950 50  0000 C CNN
F 2 "" H 7150 4100 50  0001 C CNN
F 3 "" H 7150 4100 50  0001 C CNN
	1    7150 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 4100 7150 4050
$Comp
L Conn_02x17_Odd_Even J1
U 1 1 5AC0B012
P 4400 3400
F 0 "J1" H 4450 4300 50  0000 C CNN
F 1 "Conn_02x17_Odd_Even" H 4450 2500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x17_Pitch2.54mm" H 4400 3400 50  0001 C CNN
F 3 "" H 4400 3400 50  0001 C CNN
	1    4400 3400
	1    0    0    -1  
$EndComp
Text GLabel 4900 3300 2    60   Input ~ 0
MOTOR_ON
Text GLabel 4900 3000 2    60   Input ~ 0
DRIVE0
Text GLabel 4900 3100 2    60   Input ~ 0
DRIVE1
Wire Wire Line
	4900 3000 4700 3000
Wire Wire Line
	4900 3100 4700 3100
Wire Wire Line
	4900 3300 4700 3300
Wire Wire Line
	4200 2600 3900 2600
Wire Wire Line
	3900 2600 3900 4400
Wire Wire Line
	4200 2700 3900 2700
Connection ~ 3900 2700
Wire Wire Line
	4200 2800 3900 2800
Connection ~ 3900 2800
Wire Wire Line
	4200 2900 3900 2900
Connection ~ 3900 2900
Wire Wire Line
	4200 3000 3900 3000
Connection ~ 3900 3000
Wire Wire Line
	4200 3100 3900 3100
Connection ~ 3900 3100
Wire Wire Line
	4200 3200 3900 3200
Connection ~ 3900 3200
Wire Wire Line
	4200 3300 3900 3300
Connection ~ 3900 3300
Wire Wire Line
	4200 3400 3900 3400
Connection ~ 3900 3400
Wire Wire Line
	4200 3500 3900 3500
Connection ~ 3900 3500
Wire Wire Line
	4200 3600 3900 3600
Connection ~ 3900 3600
Wire Wire Line
	4200 3700 3900 3700
Connection ~ 3900 3700
Wire Wire Line
	4200 3800 3900 3800
Connection ~ 3900 3800
Wire Wire Line
	4200 3900 3900 3900
Connection ~ 3900 3900
Wire Wire Line
	4200 4000 3900 4000
Connection ~ 3900 4000
Wire Wire Line
	4200 4100 3900 4100
Connection ~ 3900 4100
Wire Wire Line
	4200 4200 3900 4200
Connection ~ 3900 4200
$Comp
L GND #PWR03
U 1 1 5AC0B365
P 3900 4400
F 0 "#PWR03" H 3900 4150 50  0001 C CNN
F 1 "GND" H 3900 4250 50  0000 C CNN
F 2 "" H 3900 4400 50  0001 C CNN
F 3 "" H 3900 4400 50  0001 C CNN
	1    3900 4400
	1    0    0    -1  
$EndComp
$Comp
L BC817 Q2
U 1 1 5AC0B4CA
P 7400 3300
F 0 "Q2" H 7600 3375 50  0000 L CNN
F 1 "BC817" H 7600 3300 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 7600 3225 50  0001 L CIN
F 3 "" H 7400 3300 50  0001 L CNN
	1    7400 3300
	1    0    0    -1  
$EndComp
$Comp
L BC817 Q1
U 1 1 5AC0B549
P 7050 3850
F 0 "Q1" H 7250 3925 50  0000 L CNN
F 1 "BC817" H 7250 3850 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 7250 3775 50  0001 L CIN
F 3 "" H 7050 3850 50  0001 L CNN
	1    7050 3850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
